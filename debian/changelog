xfonts-jisx0213 (0+20040511-8) unstable; urgency=medium

  * debian/copyright
    - Use secure copyright file specification URI.
  * debian/control
    - Set Build-Depends: debhelper-compat (= 13)
    - Set Standards-Version: 4.5.1
    - Add Rules-Requires-Root: no
    - Move Section: fonts from x11

 -- Hideki Yamane <henrich@debian.org>  Wed, 30 Jun 2021 22:40:49 +0900

xfonts-jisx0213 (0+20040511-7.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 14:44:56 +0100

xfonts-jisx0213 (0+20040511-7) unstable; urgency=medium

  * debian/compat
    - set 11
  * debian/control
    - set Build-Depends: debhelper (>= 11)
    - drop Homepage: field, it's unrearchable now
    - fix Section: x11
    - set "Multi-Arch: foreign" and add "Suggests: xserver"
    - set Standards-Version: 4.1.3
    - move Vcs-* to salsa.debian.org
    - move package under fonts-team umbrella
  * debian/README.source
    - fix wording (Debian package maintainer)
  * debian/{rules,install,dirs,clean}
    - split rules to each file
  * debian/.gitlab-ci.yml
    - add simple CI

 -- Hideki Yamane <henrich@debian.org>  Sat, 17 Mar 2018 08:48:55 +0900

xfonts-jisx0213 (0+20040511-6) unstable; urgency=medium

  * Update debhelper compat version to 9
  * Use dh_prep instead of dh_clean -k
  * Update Vcs-Browser to https
  * Update Standards-Version to 3.9.8
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 09 Jan 2017 10:39:04 +0900

xfonts-jisx0213 (0+20040511-5) unstable; urgency=medium

  * Add Vcs-Git and Vcs-Browser
  * Update debian/copyright to copyright-format 1.0
  * New targets build-arch and build-indep
  * Update Standards-Version to 3.9.5

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 25 Aug 2014 22:30:36 +0900

xfonts-jisx0213 (0+20040511-4) unstable; urgency=low

  * Rebuild to cleanup the fonts.alias file at postrm.
  * debian/README.*:
    - Rename debian/README.Debian-source to debian/README.source.
    - Move `How to generate K12-2004-1' from debian/README.Debian to
      debian/README.source.
  * debian/control:
    - Set Section to `fonts'.
    - Update Standards-Version to 3.9.2.
  * debian/copyright: Switch to the DEP-5 format.
  * Switch to the dpkg-source 3.0 (quilt) format.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 18 May 2011 22:44:53 +0900

xfonts-jisx0213 (0+20040511-3) unstable; urgency=low

  * debian/control:
    - Move `Homepage:' from Description to the header.
    - Set Standards-Version to 3.7.3.
    - Build-Depends: debhelper (>= 6).
  * debian/compat: 5 -> 6.
  * debian/post*, debian/pre*: Removed.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 26 Mar 2008 23:11:50 +0900

xfonts-jisx0213 (0+20040511-2) unstable; urgency=low

  * debian/README.Debian: Typo fix.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Tue,  9 Jan 2007 23:09:44 +0900

xfonts-jisx0213 (0+20040511-1) unstable; urgency=low

  * Initial release. (closes: #403502)

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 22 Dec 2006 22:49:38 +0900
